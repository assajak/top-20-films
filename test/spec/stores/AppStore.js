'use strict';

describe('AppStore', () => {
  let store;

  beforeEach(() => {
    store = require('stores/AppStore.js');
  });

  it('should be defined', () => {
    expect(store).toBeDefined();
  });
});
