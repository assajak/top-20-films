'use strict';

describe('RunActionCreators', () => {
  let action;

  beforeEach(() => {
    action = require('actions/RunActionCreators.js');
  });

  it('should be defined', () => {
    expect(action).toBeDefined();
  });
});
