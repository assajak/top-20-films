'use strict';

describe('ImbApp', () => {
  let React = require('react/addons');
  let ImbApp, component;

  beforeEach(() => {
    let container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    ImbApp = require('components/ImbApp.js');
    component = React.createElement(ImbApp);
  });

  it('should create a new instance of ImbApp', () => {
    expect(component).toBeDefined();
  });
});
