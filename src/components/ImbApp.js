'use strict';

var React = require('react/addons');
//var ReactTransitionGroup = React.addons.TransitionGroup;
var FilmsStore = require('../stores/FilmsStore');
var Tabs = require('react-simpletabs');
var PieChart = require('react-simple-pie-chart');
var cx = React.addons.classSet;
var ActionCreator = require('./../actions/RunActionCreators');


//require('normalize.css');
require('../styles/main.css');
require('../../node_modules/react-simpletabs/dist/react-simpletabs.css');

var FilmComponent = React.createClass({
    getInitialState: function(){
      var favoritesFilms = FilmsStore.getFavoritesFilmId();

      return {favorite: (favoritesFilms.indexOf(this.props.film.idIMDB) !== -1) ? true : false};
    },
    renderDirectors: function () {
        var directorsList = [];

        for (var id in this.props.film.directors) {
            directorsList.push(
                <div>
                    <a target="blank" href={'http://www.imdb.com/name/' + this.props.film.directors[id].nameId}>{this.props.film.directors[id].name}</a>
                </div>
            );
        }

        return directorsList;
    },
    addToFavorites: function () {
        var fav = !this.state.favorite;
      this.setState({favorite: fav});
        ActionCreator.addToFavorite(this.props.film, fav);
    },
    render: function () {
        var classList = cx({
            favorite: true,
            full: this.state.favorite
        });

        return (
            <div>

                <div className="float-left">
                    <img src={this.props.film.urlPoster}/>
                </div>

                <div className="float-left film-info-wrapper">
                    <div>
                        Year: {this.props.film.year}
                    </div>
                    <div>
                        Rating: {this.props.film.rating}
                    </div>
                    <div>
                        Film Name: {this.props.film.title}
                    </div>
                    <div>
                        Genres: {this.props.film.genres.join(', ')}
                    </div>
                    <div>
                        Countries: {this.props.film.countries.join(', ')}
                    </div>

                    <div>
                        <div onClick={this.addToFavorites} className={classList}></div>
                    </div>

                    <div>{this.renderDirectors()}</div>
                </div>

                <div className="clear"></div>


                <hr/>
            </div>
        );
    }
});

var TopFilms = React.createClass({
    getInitialState: function () {
        return {films: []};
    },
    _onChange: function () {
        this.forceUpdate();
    },
    componentDidMount() {
        FilmsStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        FilmsStore.removeChangeListener(this._onChange);
    },
    renderListOfFilms: function (filmsList) {
        var listOfFilms = [];

        for (var id in filmsList) {
            listOfFilms.push(
                <FilmComponent film={filmsList[id]}/>
            );
        }

        return listOfFilms;
    },
    render: function () {
        var filmsList = FilmsStore.getFilms();

        return (<div> { this.renderListOfFilms(filmsList) } </div>);
    }
});
var FavoritesFilms = React.createClass({
    getInitialState: function () {
        return {films: []};
    },
    _onChange: function () {
        this.forceUpdate();
    },
    componentDidMount() {
        FilmsStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function () {
        FilmsStore.removeChangeListener(this._onChange);
    },
    renderListOfFilms: function (filmsList) {
        var listOfFilms = [];

        for (var id in filmsList) {
            listOfFilms.push(
                <FilmComponent film={filmsList[id]} key={filmsList[id].idIMDB}/>
            );
        }

        return listOfFilms;
    },
    render: function () {

        var filmsList = FilmsStore.getFavoritsFilmList();

        return (<div> { this.renderListOfFilms(filmsList) } </div>);
    }
});

var FilmsGraphComponent = React.createClass({
    getInitialState: function () {
        return {films: []};
    },
    _onChange: function () {
        this.forceUpdate();
    },
    componentDidMount() {
        FilmsStore.addChangeListener(this._onChange);
    },
    renderMapLegend: function(graphData){
        var legend = [];

        for (var id in graphData){
            legend.push(
                <div className="float-left legend-item">
                    <h4>{graphData[id].decimes} ({graphData[id].value})</h4>
                    <div className="legendColor" style={{'background-color': graphData[id].color}}></div>
                </div>
            );
        }

        return legend;
    },
    render: function () {

        var graphData = FilmsStore.getCountOfFilmsByYear();

        return (
            <div>
                <div className="pie-wrapper">
                    <PieChart width="100" height="100"
                        slices={graphData}
                    />
                </div>

                <div>
                    <h3>Legend:</h3>
                    {this.renderMapLegend(graphData)}
                    <div className="clear"></div>

                </div>
            </div>
        );
    }
});


var ImbApp = React.createClass({
    render: function () {

        return (

            <Tabs>
                <Tabs.Panel title='Top films'>

                    <div>
                        <TopFilms/>
                    </div>
                </Tabs.Panel>
                <Tabs.Panel title='Top pie'>
                    <h2>Pie from year</h2>
                    <div>
                        <FilmsGraphComponent/>
                    </div>
                </Tabs.Panel>
                <Tabs.Panel title='Favorites'>
                    <h2>Favorites</h2>
                    <FavoritesFilms/>
                </Tabs.Panel>
            </Tabs>
        );
    }
});


module.exports = ImbApp;
