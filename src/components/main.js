'use strict';

var ImbApp = require('./ImbApp');
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var AppStore = require('./../stores/AppStore');

var content = document.getElementById('content');

var Index = React.createClass({
    getInitialState: function(){
      return {'showSpener': true};
    },
    _onChange: function(){
        this.setState({showSpener: !this.state.showSpener});
    },
    componentDidMount() {
        AppStore.addChangeListener(this._onChange);
    },
    renderSpener: function(){

        if (!this.state.showSpener){
            return null;
        }

        return (
            <div className="spiner-block">
                <div className="spiner-image">
                    <img src='./../images/16-loader.GIF'/>
                </div>
            </div>
        );
    },
    render: function () {

        return (
            <div>
                {this.renderSpener()}
                <RouteHandler/>
            </div>
        );
    }
});

var Routes = (
    <Route handler={Index}>
        <Route name="/" handler={ImbApp}/>
    </Route>
);

Router.run(Routes, function (Handler) {
    React.render(<Handler/>, content);
});
