/**
 * Created by assajak on 24.08.2015 15:44.
 * E-mail: assajak.wtf@gmail.com.
 */

module.exports = {
    ActionTypes: {
        ADD_TO_FAVORITES: 'addToFavorite',
        START_FETCH_DATA: 'start_fetch_data',
        END_FETCH_DATA: 'end_fetch_data'
    }
};
