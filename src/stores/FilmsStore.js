'use strict';

var Constants = require('./../constants/app');
var assign = require('object-assign');
var ImbAppDispatcher = require('../dispatcher/ImbAppDispatcher');
var BaseStore = require('./BaseStore');
var $ = require('jQuery');
var ActionCreator = require('./../actions/RunActionCreators');

var FilmsStore = assign({}, BaseStore, {
    films: [],
    getFavoritsFilmList: function(){
        if (this.films.length === 0){
            this.fetchFilmsFromApi();
            return;
        }

        var favorites = [];
        var favoritesFilms = FilmsStore.getFavoritesFilmId();


        for (var id in this.films){
            if(favoritesFilms.indexOf(this.films[id].idIMDB) === -1){
                continue;
            }

            favorites.push(this.films[id]);
        }

        return favorites;
    },
    getFavoritesFilmId: function(){
        var favorites = JSON.parse(localStorage.getItem('favorites'));
      return (favorites) ? favorites : [];
    },
    setFavoritesId: function(arrayOfIds){
        return localStorage.setItem('favorites', JSON.stringify(arrayOfIds.filter( onlyUnique )));
    },
    addFilmToFavorites: function(data){
        var favorites = this.getFavoritesFilmId();
       if (data.status){
           favorites.push(data.film.idIMDB);
       }else{
           var i = favorites.indexOf(data.film.idIMDB);
           if(i !== -1) {
               favorites.splice(i, 1);
           }

           FilmsStore.emitChange();
       }

        this.setFavoritesId(favorites);
    },
    getFilms: function() {

        if (this.films.length === 0){
            this.fetchFilmsFromApi();
        }

        return this.films;
    },
    getCountOfFilmsByYear: function(){
        if (!this.films){
            this.fetchFilmsFromApi();
            return [];
        }

        var years = [];
        var decimes = [];

        for (var id in this.films){
            years.push(parseInt(this.films[id].year, 10));
        }



        var min = Math.min.apply(null, years),
            max = Math.max.apply(null, years);

        var startDecime = Math.round(min/10)*10;
        var endDecime = Math.ceil(max/10)*10;

       for (var i = startDecime; i <= endDecime; i = i+10){

           var countOfFilmsInCurrentDecimes = 0;

           for (var id in years){
               if (Math.round(i/10) === Math.round(years[id]/10)){
                   countOfFilmsInCurrentDecimes++;
               }
           }

           decimes.push({
               color: getRandomColor(),
               value: countOfFilmsInCurrentDecimes,
               decimes: i
           })
       }

        return decimes;
    },
    fetchFilmsFromApi: function(){

      ActionCreator.showLoader();

      var self = this;
      $.ajax({
          url: 'http://www.myapifilms.com/imdb/top',
          data: {
              'format': 'JSON',
              'start': '1',
              'end': '20',
              'data': 'F',
              'token': '28c9babc-a406-441c-9a4e-e195e1098ab6',
              'lang': 'ru-ru'
          },
          success: function(e){
              self.films = e;
              FilmsStore.emitChange();

              ActionCreator.hideLoader();
          }
      })
    },
    // register store with dispatcher, allowing actions to flow through
    dispatcherIndex: ImbAppDispatcher.register(function(payload) {
        var action = payload;

        switch(action.type) {
            case Constants.ActionTypes.ADD_TO_FAVORITES:
                FilmsStore.addFilmToFavorites(action.data);
            break;
        }
    })

});

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

module.exports = FilmsStore;
