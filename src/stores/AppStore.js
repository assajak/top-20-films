'use strict';

var assign = require('object-assign');
var ImbAppDispatcher = require('../dispatcher/ImbAppDispatcher');
var Constants = require('./../constants/app');
var BaseStore = require('./BaseStore');

var AppStore = assign({}, BaseStore, {

    speener: function(){
        AppStore.emitChange();
    },
    // register store with dispatcher, allowing actions to flow through
    dispatcherIndex: ImbAppDispatcher.register(function(payload) {
        var action = payload;

        switch(action.type) {
            case Constants.ActionTypes.START_FETCH_DATA:
                AppStore.speener('on');
                break;

            case Constants.ActionTypes.END_FETCH_DATA:
                AppStore.speener('off');
                break;
        }
    })

});

module.exports = AppStore;
