'use strict';

var AppDispatcher = require('./../dispatcher/ImbAppDispatcher');
var Constants = require('./../constants/app');


var RunActionCreators = {
    addToFavorite: function(film, status){


        AppDispatcher.dispatch({
            type: Constants.ActionTypes.ADD_TO_FAVORITES,
            data: {film: film, status: status}
        });
    },
    showLoader: function(){
        AppDispatcher.dispatch({
            type: Constants.ActionTypes.START_FETCH_DATA
        });
    },
    hideLoader: function(){
        AppDispatcher.dispatch({
            type: Constants.ActionTypes.END_FETCH_DATA
        });
    }
};

module.exports = RunActionCreators;
